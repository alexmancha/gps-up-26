# SISTEMA UPC4ALL - GLOSSARI #

Per cada terme:

- Usuari: Persona que té un contacte directe amb l'aplicació i que l'usa per treure'n un benefici o aconseguir-ne un objectiu.

- Administrador: Usuari amb un rol que li permet gestionar els diversos continguts de la plataforma UPC4All, encara que no en sigui el creador.

- Admin: v. Administrador.

- Dispositiu mòbil: Tipus d'ordinador de mida petita, amb connexió a internet i capacitats de processament suficients com per dur a terme tasques generals. Els dispositius mòbils inclouen, però no es limiten, a telèfons intel·ligents, tauletes i rellotges intel·ligents.

- Comunitat universitària: Conjunt de col·lectius relacionats directament amb la gestió i ús d'una universitat, tals com estudiants, professors o personal de suport a la docència, la recerca i la gestió, així com el govern i la legislació de la pròpia universitat.

- Proposta: Idea manifestada a la plataforma UPC4All per part d'un usuari que es comparteix amb la resta d'usuaris perquè la puguin avaluar i mostrar-hi conformitat o disconformitat.

- Idea: v. Proposta.

- Comentari: Resposta que un usuari fa a una proposta presentada per un altre usuari o per ell mateix.