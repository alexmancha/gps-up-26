# SISTEMA UPC4ALL - VISIÓ #

## 1. INTRODUCCIÓ ##

La presa de decisions és un procés essencial per al millor desenvolupament possible d'una institució com és una universitat. Però aquestes decisions no poden ser preses per un petit grup de persones, que és probable que no considerin tots els aspectes que comporta la presa d'una decisió des de diferents punts de vista.

Tenint en consideració això, volem desenvolupar una aplicació que permeti als membres de la comunitat universitària participar en la presa d'aquelles decisions per a les quals estiguin qualificats.

Aquest sistema permetrà als diferents components de la comunitat presentar i valorar propostes, la factibilitat de les quals serà gestionada per un comité compost per membres dels diferents àmbits de la comunitat. Els usuaris podran votar entre diferents propostes dels diversos aspectes sobre els quals s'hagin de prendre decisions així com comentar la proposta i generar debat.

## 2. EL PROBLEMA ##

Hi ha un gran nombre de decisions importants que la direcció de la UPC ha de prendre per tal de mantenir i millorar el funcionament de la universitat.

Tant la comunicació de les decisions a prendre com la presa de decisions en sí suposen un problema actualment, ja sigui per la dificultat amb la que es troba la universitat per poder fer arribar la informació adient a aquelles persones que és important que participin en la presa de decisions, ja siguin alumnes, docents o altres, com pel fet de que el procés
de presa de decisions no és suficientment àgil.

Per tant, el problema radica en que la participació de la comunitat universitària en la presa de decisions dista molt de ser la òptima. És de vital importància que la participació en la presa de decisions involucri tots aquells individus capacitats per a decidir-ne al respecte, per tal de tenir una universitat més plural, justa i adaptada a les necessitats i acords dels seus integrants. 

A més, millorar aquest aspecte no és només una qüestió d'eficiència sinó també de docència, ja que es dóna l'exemple de que tota institució ha de comptar amb la participació dels seus integrants i associats, obtenint així una millora social i probablement en l'economia, ja que tot individu se sent més a gust i treballa de forma més eficient sabent que la seva opinió es té en compte. 

## 3. PARTS INTERESSADES ##

1. *Rectorat/Direcció de la UPC*. La direcció de la universitat té un alt interès en el sistema, ja que permetria avaluar el suport que reben les diferents opcions a l'hora de prendre decisions. La seva responabilitat principal consisteix en donar la màxima informació sobre les diferents opcions de cada presa de decisions, per tal de que el sistema pugui comunicar el millor possible en què consisteix cada una i que siguin el menys ambigües possible. Això és important ja que la direcció és qui prèn la decisió final.

2. *Estudiants*. Els estudiant volen tenir més participació en la presa de decisions de la universitat, cosa que els fa estar interessats en que aquesta aplicació millori la seva participació. La seva responsabilitat principal serà la d'assegurar-se de promoure l'ús del sistema i proporcioanr feedback per tal de millorar-ne l'ús, aparença, etc.

3. *Professorat*. Similar als estudiants, el professorat vol tenir més presència en la presa de decisions que els concerneixen, pel que una aplicacuó d'aquest tipus els ajudaria a conseguir-ho. La seva responsabilitat seria la de plantejar noves decisions en l'àmbit acadèmic i donar feedback.

4. *Personal no docent*. Alguns components del personal no estudiantil ni docent haurien de poder participar en la presa de decisions que els afectin.

## 4. EL PRODUCTE ##

### 4.1. Perspectiva del producte ###

Escollim un model d'aplicació mòbil juntament amb un servei web, permetent que l'aplicació mòbil es comuniqui amb la base de dades del servei web a través d'una API per a gestionar les propostes.
 
A continuació es mostra un esquema de l'arquitectura del sistema presentat:

![UPC4All esquema](https://bytebucket.org/alexmancha/gps-up-26/raw/47a95838bd831c8edad57ee2e6e41b04ddb02e65/upc4all-schema.png)


### 4.2. Descripció del producte ###

1. *Donar d'alta una proposta en el forum*
2. *Validar una proposta en el forum (Només un administrador)*
3. *Crear un tema oficial de debat (Només un administrador)*
4. *Donar de baixa una proposta en el fòrum sobre una proposta de solució (Només l'usuari que l'ha donat d'alta o un administrador)*
5. *Donar de baixa un tema oficial (Només un administrador)*
5. *Editar una proposta en el fòrum sobre una solució (Només l'usuari que l'ha donat d'alta o un administrador)*
6. *Votar una proposta concreta en un tema oficial/forum de debat*
7. *Debatre una proposta en el fòrum i donar la teva opinió*
8. *Analitzar els resultats obtinguts*


### 4.3. Supòsits de funcionament ###

1. Suposem que els usuaris potencials de l'aplicació la descarregaran de les botigues virtuals d'aplicacions de cada plataforma per utilitzar-la des dels seus dispositius mòbils.

2. Suposem que els usuaris accediran al sistema de forma regular i hi participaran a través de la creació de noves propostes i la valoració de propostes ja existents.

3. Suposem que els òrgans de decisió de la UPC donaran el vistiplau al sistema i n'aprofitaran el coneixement obtingut per millorar la universitat.

 
### 4.4. Dependències sobre altres sistemes ###

1. *Intranet de la universitat:* Permetria als usuaris (estudiants/docents) utilitzar el sistema utilitzant el seu perfil de la intranet de la universitat, permetent una major cohesió en les dades. En cas que la intranet quedés innaccessible els usuaris no podrien validar-se per accedir a l'aplicació.

  
### 4.5. Altres requisits (requisits no funcionals) ###

1. *Facilitat d'ús:* L'aplicació ha de ser senzilla i intuitiva, ja l'objectiu és facilitar la participació en els processos de presa dedecisions.
2. *Fiabilitat i disponibilitat:* El sistema ha d'estar disponible i ser accessible en qualsevol moment.
3. *Requeriments d'aparença:* L'aplicació ha de ser agradable visualment, ja que bona part dels usuaris són estudiants i valoren molt aquest aspecte.
4. *Requeriments d'integritat:* Les dades amb les que tracta el sistema s'han de mantenir íntegres, evitant qualsevol informació erònea que pugui donar-se.
5. *Velocitat i latència:* Al ser una aplicació amb força usuaris i tractant decisions importants, és important que l'aplicació respongui el més ràpid possible a les peticions dels usuaris.