# SISTEMA UPC4ALL - PLA D'ITERACIÓ INCEPTION #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

Aquesta iteració, la primera del projecte i l'única de la fase de Inception, té una durada prevista de dues setmanes, des del 18 de setembre fins el 6 d'octubre.

## 2. COBERTURA DE CASOS D'ÚS ##

Tal i com hem indicat al pla de desenvolupament del software, en acabar la iteració corresponent a la fase de Inception, tots els casos d'ús estaran identificats.

La iteració d'inception ens servirà per esbossar alguns dels casos d'ús principals de l'aplicació, amb l'objectiu de poder identificar de primera mà i amb marge de temps alguns requeriments o restriccions que ens haguessin pogut passar per alt.

A més a més, dos casos d'ús (Login i Logout) estaran també refinats i en un estat suficientment avançat com per poder passar a desenvolupament.

## 3. ACTIVITATS ##

Tindrem tantes activitats com objectius volem aconseguir abans de començar la fase de Elaboration.

**Codi**|**Activitats**|**Temps**|**Precedències**
:-----:|:-----:|:-----:|:-----:
A01|Concepció del nou projecte|1 dia|N/A
A02|Anàlisi del problema a resoldre|1 dia|A01
A03|Definir pla de desenvolupament de software|1 dia|A05
A04|Definir pla d'iteracions|1 dia|A05
A05|Reclutar personal|1 dia|A09
A06|Avaluar estat del negoci|1 dia|A02
A07|Determinar impacte econòmic|1 dia|A06
A08|Identificar i avaluar riscs|1 dia|A07
A09|Definir rols i responsabilitats|1 dia|A08
A10|Desenvolupar la visió|1 dia|A11,A12
A11|Identificar requisits funcionals|2 dies|A05
A12|Identificar requisits no funcionals|1 dia|A05
A13|Identificar actors del sistema|1 dia|A10
A14|Definir casos d'ús|3 dies|A10
A15|Anàlisi de l'arquitectura|2 dies|A10
A16|Disseny dels casos d'ús|3 dies|A13,A14
A17|Configurar eines|1 dia|A15
A18|Desenvolupar guies de proves|1 dia|A16
 
## 4. DIAGRAMA DE GANTT ##

![Diagrama de Gantt](https://bytebucket.org/alexmancha/gps-up-26/raw/22ba8fbc13474a8c34a303e41c652d9de559ad06/gantt.png)

## 5. DIAGRAMA DE PERT ##

![Diagrama Pert](https://bytebucket.org/alexmancha/gps-up-26/raw/c8927d37fa0a3684c7e1926cc013303b95bf6229/pert.png)

El camí crític suma 13 dies (A01 -> A02 -> A09 -> A05 -> A11 -> A10 -> A14 -> A16).
