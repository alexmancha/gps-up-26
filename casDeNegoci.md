# SISTEMA UPC4ALL - CAS DE NEGOCI #


## 1. DESCRIPCIÓ DEL PRODUCTE ##

Donat que hi ha un problema en la presa de decisions de la universitat, ens proposem a fer una aplicació per a mòbils i web on tot el col·lectiu universitari ( professors, estudiants ) pugui votar-les i proposar-les. 
D'aquesta manera fem que tothom hi pugui participar i el resultat sigui analitzat per la direcció i tingut en compte a l'hora de prendre una decisió.

## 2. CONTEXT DE NEGOCI ##

El sector de negoci en el qual treballem és el social i acadèmic juntament amb telecomunicacions.
Serà un producte intern, ja que el fem només per a la universitat UPC i no estarà disponible ( en un principi ) per a cap altra universitat.

## 3. OBJECTIUS DEL PRODUCTE ##

1. *Unificació*. Permetre que tots aquells temes en els que es desitgi l'opinió de la comunitat universitària estiguin a l'abast d'aquesta de forma ràpida i senzilla.
2. *Integració de la comunitat*. Desitgem que la comunitat universitària al complet pugui participar en la presa de decisions.
3. *Millora dels valors socials*. Demostrant que la participació dels integrants d'una comunitat en les preses de decisions i enquestes és essencial per tal de tenir la millor comunitat possible. 
4. *Promoure l'ús d'aquest tipus d'aplicacions*. Ajudar a que altres comunitats, com empreses privades, facin ús d'aquest tipus d'aplicacions que proporcionin feedback per tal de millorar aquestes comunitats.

## 4. RESTRICCIONS ##

1. *Sistemes operatius de l'aplicació*. Només es desenvoluparà l'aplicació per a sistemes operatius Android i iOS.
2. *Versions del sistema operatiu de l'aplicació mòbil*. Desenvoluparem l'aplicació mòbil per a les dues últimes versions dels sistemes operatius mòbils iOS i Android. La compatibilitat amb versions anteriors no està assegurada.
3. *Últimes versions dels navegadors web*. Desenvoluparem l'aplicació web assegurant una àmplia compatibilitat amb els principals navegadors web existents en les seves últimes versions (Chrome, Firefox, Safari, Edge i Opera).
4. *Estàndard Rest API*. La comunicació que farà l'aplicació amb la base de dades serà a partir de una API Restful [*1*]. Això implica que haurà de seguir els estàndards d'operacions d'aquesta arquitectura.
5. *Ser membre de la UPC*. L'aplicació està destinada només per a usuaris de la UPC.
6. *Restricció lingüística*. Per tal de desenvolupar-la de forma tan inclusiva com sigui possible amb els recursos disponibles, l'aplicació comptarà amb la interfície traduïble en català, castellà i anglès.
7. *Connexió a internet*. Per fer servir l'aplicació l'usuari haurà d'estar connectat a internet.

## 5. PREVISIÓ FINANCERA ##

En un principi l'aplicació no està dissenyada per ser comercialitzada per tant serà per a ús intern de la universitat. Avaluant els beneficis que ens proporcionaria aquesta aplicació tindríem beneficis socials, ja que si s'aconsegueix demostrar que una millor comunicació proporciona un millor desenvolupament de la comunitat, aconseguim inculcar valors positius en les noves generacions d'estudiants. També s'obtindrien beneficis econòmics indirectes, ja que si aquesta aplicació millora el rendiment de la universitat en assumptes de presa de decisions, pot millorar la reputació de la universitat, fent que probablement entitats externes vulguin començar a col·laborar amb la universitat. I per acabar tindríem els beneficis ambientals, obtenibles gràcies al fet que aquesta aplicació té potencial per a oferir propostes més sostenible.


## 6. RECURSOS ##

[1] [REST API Tutorial](http://restfulapi.net/)
