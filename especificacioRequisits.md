# SISTEMA NOM - ESPECIFICACIÓ DE REQUISITS DEL SOFTWARE #

## 1. ESPECIFICACIÓ FUNCIONAL ##

### 1.1. Diagrama de casos d'ús

Casos d'ús de l'usuari normal:

![Casos ús usuari](https://bytebucket.org/alexmancha/gps-up-26/raw/9160062ad1f760b83ed758726b509f23ef411af2/usecaseuser.png)


Casos d'ús de l'administrador:

![Casos ús administrador](https://bytebucket.org/alexmancha/gps-up-26/raw/902dcb7ce9285f64dffcfadb8eacd41ce5a98566/usecaseadmin.png)


### 1.2. Descripció individual dels casos d'ús


### __Usuari normal (estudiant/professor):__ ###


#### Grup 1: Gestió de comptes: ####

__UC01-Login:__

L’usuari inicia sessió a l’aplicació amb les seves dades de la intranet de l’UPC.

__UC02-Logout:__

L’usuari tanca sessió per a deixar d’utilitzar l’aplicació.


#### Grup 2: Gestió de propostes: ####

__UC03-Crear una proposta de solució en una categoria:__

Dins d’una categoria (p.e. el disseny de la carpeta de la facultat) un usuari dóna d’alta la seva proposta de solució per a el tema en qüestió.

__UC04-Donar de baixa una proposta de solució (Només l'usuari que l'ha donat d'alta):__

L’usuari elimina la proposta que ha donat d’alta en una categoria. Aquesta s’eliminarà juntament amb tots els comentaris associats a aquesta.

__UC05-Editar una proposta de solució (Només l'usuari que l'ha donat d'alta):__

L’usuari edita la proposta que ha donat d’alta en una categoria. El sistema informarà sobre els canvis realitzats a qualsevol usuari que hagi participat en la proposta.


#### Grup 3: Interaccions: ####

__UC06-Votar una proposta concreta:__

L’usuari pot votar una proposta una única vegada per tal de mostrar el seu suport. (Quantes propostes pot votar per tema/categoria?).

__UC07-Afegir comentaris a una proposta per tal de debatir-ne possibles modificacions amb l'autor de la proposta:__

L’usuari pot afegir comentaris a una proposta, per tal de donar el seu punt de vista sobre la proposta i comentar-ne possibles modificacions.

__UC08-Visualitzar totes les propostes en les que s’ha participat:__

L’usuari obté un llistat on consten totes aquelles propostes que ha valorat i/o comentat.


#### Grup 4:  Anàlisi: ####

__UC09-Analitzar els resultats obtinguts:__

L’ usuari accedeix a la informació sobre l’estat d’una categoria i les seves estadístiques: Quines propostes s’han presentat en aquella categoria, quines han estat les més votades, els comentaris més rellevants, etc.


### Administrador: ###

__UC10-Validar una proposta de solució en una categoria:__

Un administrador pot validar una proposta donada d’alta per un usuari per tal de que pugui ser vista, votada i comentada per la resta d’usuaris de l’aplicació.

__UC11-Donar de baixa una proposta de solució:__

Un administrador dóna de baixa una proposta que un usuari hagi donat d’alta.

__UC12-Editar una proposta de solució:__

Un administrador modifica una proposta donada d’alta per un usuari. Les modificacions seràn comunicades als usuaris pel sistema.

__UC13-Analitzar els resultats obtinguts:__

Un usuari ha de poder accedir a la informació sobre l’estat d’una categoria i les seves estadístiques: Quines propostes s’han presentat en aquella categoria, quines han estat les més votades, els comentaris més rellevants, etc.


## 2. ESPECIFICACIÓ NO FUNCIONAL ##

__RNF1: Facilitat d'ús:__

*Descripció*: L'aplicació ha de ser senzilla i intuitiva, ja l'objectiu és facilitar la participació en els processos de presa de decisions.

*Justificació*: Com que l'aplicació serà utilitzada per un gran nombre de persones, cal que la dificultat d'ús sigui molt baixa, encara que, presumiblement, el nivell tècnic dels usuaris d'una universitat tècnica serà relativament alt.

*Condició de satisfacció*: Una enquesta anònima hauria de mostrar que un 75% dels usuaris fan ús sovint de l'aplicació després de 30 dies.


__RNF2: Fiabilitat i disponibilitat:__

*Descripció*: El sistema ha d'estar disponible i ser accessible en qualsevol moment.

*Justificació*: Només un sistema fiable oferirà una imatge de valor, a més de permetre el seu ús en qualsevol moment.

*Condició de satisfacció*: El sistema ha d'estar disponible les 24 hores del dia, els 365 dies de l'any, amb parades previstes i anunciades de funcionamente per manteniment.


__RNF3: Aparença:__

*Descripció*: L'aplicació ha de ser agradable visualment, ja que bona part dels usuaris són estudiants i valoren molt aquest aspecte.

*Justificació*: Una aplicació d'aparença agradable facilitarà el seu ús i animarà els usuaris a seguir utilitzant-la. 

*Condició de satisfacció*: Els usuaris (en un rang ampli d'edats) hauria de poder començar a utilitzar l'aplicació en els primers minuts després d'entrar-hi en contacte.


__RNF4: Integritat:__

*Descripció*: Les dades amb les que tracta el sistema s'han de mantenir íntegres, evitant qualsevol informació errònea que pugui donar-se.

*Justificació*: És imprescindible que l'aplicació transmeti una sensació de fiabilitat i de robustesa de cara als usuaris.

*Condició de satisfacció*: El sistema ha de prevenir en tot moment l'inserció de dades incorrectes o que puguin provocar errors d'integritat de les bases de dades. Aquests errors comprenen des de que un usuari pugui votar més d'un cop a que no es realitzin correctament les votacions o altes.


__RNF5: Velocitat i latència:__

*Descripció*: Al ser una aplicació amb força usuaris i tractant decisions importants, és important que l'aplicació respongui el més ràpid possible a les peticions dels usuaris.

*Justificació*: Ja que el sistema s'ha dissenyat per a facilitar els processos de participació en preses de decisions, totes les funcionalitats del sistema a l'abast dels usuari han de respondre el més ràpid possible.

*Condició de satisfacció*: El sistema hauria de mostrar una resposta en no més d'un segon a la consulta o modificació realitzada per l'usuari en el 90% dels casos. Una resposta per part del sistema no pot prendre més de 3 segons.

## 3. MOCK-UPS ##

### Mockup - Creació de proposta

![Mockup - Creació de proposta](https://bytebucket.org/alexmancha/gps-up-26/raw/1f5c4ef4d846c246c9bf230d2cdd8701ee9f2916/wireframe-creacio-proposta.png)


### Mockup - Detall d'una proposta

![Mockup - Detall d'una proposta](https://bytebucket.org/alexmancha/gps-up-26/raw/1f5c4ef4d846c246c9bf230d2cdd8701ee9f2916/wireframe-detall-proposta.png)


### Mockup - Perfil usuari

![Mockup - Perfil d'usuari](https://bytebucket.org/alexmancha/gps-up-26/raw/1f5c4ef4d846c246c9bf230d2cdd8701ee9f2916/wireframe-perfil-usuari.png)

## 4. RECURSOS ##

[1] http://www.volere.co.uk/
[2] http://www.uml-diagrams.org/use-case-diagrams-examples.html
