# SISTEMA UPC4ALL - PLA DE DESENVOLUPAMENT DE SOFTWARE #

## 1. ORGANITZACIÓ I EQUIP ##


| Rol        | Descripció           | Quantitat  |
| ------------- |-------------| :--------:| :-----:|
| Arquitecte de software      | Encarregat de definir l'arquitectura bàsica de la solució tècnica i de mantenir una excel·lència tècnica en el projecte. | 1 |
| Cap de projecte      | Encarregat de la planificació i la supervisió del projecte, de les tasques a dur a terme i de la correcta gestió dels recursos disponibles, tant econòmics com humans i tecnològics.      | 1 |
| Analista | Encarregat de recopilar la informació necessària per desenvolupar una solució útil pels usuaris.      | 1 |
| Programador      | Encarregat d'implementar de forma eficient els dissenys proposats pel dissenyador, l'analista i el cap de projecte. | 2 |
| Dissenyador UI      | Encarregat del disseny dels diversos elements gràfics que requereixi l'aplicació.      | 1 |
| QA Tester | Encarregat de verificar la correctesa tècnica de la solució proposada i de detectar possibles anomalies de funcionament.      | 1 |


## 2. PLA DE PROJECTE ##

### 2.1. Estimació d'esforç ###

#### UAW ####
![UAW](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/UAW.PNG)

Els actors principals que interactuen amb el nostre software són els usuaris i els administradors, ambdós complexos ja que es tracta d'individus.
El valor UAW ens permet comptabilitzar el pes dels actors del sistema, tenint en compte la seva complexitat.

#### UUCW ####
![UUCW](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/uucw.PNG)

Hem assumit que la majoria dels casos d'ús que interactuen amb els servidors i les bases de dades són mitjanament complexos. Els casos d'ús que interactuen amb l'API de la FIB són els més senzills, ja que el nostre sistema no s'encarrega de la gestió de les dades. L'anàlisi dels resultats probablement sigui el més complex, ja que ha de comprovar que la informació sigui versemblant i mostrar dades representatives.
El valor UUCW ens permet comptabilitzar el pes dels casos d'ús dissenyats, tenint en compte la seva complexitat de desenvolupament.

#### TCF ####
![TCF](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/tcf.PNG)

Hem definit els pesos dels factors de complexitat tècnica segons la seva complexitat i la prioritat que té cada un per al nostre sistema amb el vaor TCF, adequant-nos als requisits de qualitat que hem definit.

#### ECF ####
![ECF](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/ecf.PNG)

Els factors d'entorn que compliquen o faciliten el desenvolupament. El més notable és la complicació del desenvolupament degut a la poca familiaritat amb UP.
El valor ECF ens permet determinar el pes d'aquests factors.

#### UCP ####
![UCP](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/UCP.PNG)

Els Use Case Points, que els hem definit com una equivalència a 20h d'esforç per punt, degut a que és el primer projecte amb UP que realitzem.

#### Estimació ####
![Estimacio](https://bytebucket.org/alexmancha/gps-up-26/raw/ffd9ba4c3de0ef720d4d9e161a9d8157b054f10c/Estimacio.PNG)


### 2.2. Estimació de cost ###

![Preu_Personal](https://bytebucket.org/alexmancha/gps-up-26/raw/e279225234f9c2289b2a7fe69c35c78d31557b38/preu_personal.PNG)

![Carrega_Treball](https://bytebucket.org/alexmancha/gps-up-26/raw/e279225234f9c2289b2a7fe69c35c78d31557b38/carrega_treball.PNG)

![Cost_Total](https://bytebucket.org/alexmancha/gps-up-26/raw/e279225234f9c2289b2a7fe69c35c78d31557b38/cost_total.PNG)


## 3. PLA DE FASES ##

 ||**Actor**|**Inception**|**Elaboration**|**Construction**|**Transition**
:-----|:-----:|:-----:|:-----:|:-----:|:-----:
Login|Usuari|Refinat|Analitzat|Complet|Complet
Logout|Usuari|Refinat|Analitzat|Complet|Complet
Alta proposta de solució|Usuari|Esbossat|Refinat|Complet|Complet
Baixa proposta de solució|Usuari|Esbossat|Refinat|Complet|Complet
Editar proposta de solució|Usuari|Identificat|Refinat|Analitzat|Complet
Votar proposta|Usuari|Esbossat|Analitzat|Complet|Complet
Afegir comentaris a una proposta|Usuari|Identificat|Esbossat|Refinat|Complet
Visualitzar totes les propostes participades|Usuari|Identificat|Esbossat|Analitzat|Complet
Validar una proposta de solució|Administrador|Esbossat|Analitzat|Complet|Complet
Donar de baixa una proposta de solució|Administrador|Esbossat|Refinat|Analitzat|Complet
Editar una proposta de solució|Administrador|Identificat|Esbossat|Analitzat|Complet
Analitzar els resultats obtinguts|Usuari/Administrador|Esbossat|Refinat|Analitzat|Complet

Proposem centrar-se inicialment en els casos d'ús de Login i Logout, que es trobaran ja en un estat refinat a l'acabar la fase d'Inception. El motiu, bàsicament, és per descobrir a temps possibles inconvenients a l'hora de connectar UPC4All amb els usuaris de la intranet de la UPC, d'on s'ha de treure la informació per poder accedir a l'aplicació.

A continuació indiquem el percentatge de casos d'ús en un determinat estat al final de cada una de les fases del projecte:

**Estat cas d’ús**|**Inception**|**Elaboration**|**Construction**|**Transition**
:-----:|:-----:|:-----:|:-----:|:-----:
Identificat|100%|100%|100%|100%
Esbossat|42%|100%|100%|100%
Refinat|17%|67%|100%|100%
Analitzat|0%|33%|92%|100%
Complet|0%|0%|50%|100%

![Evolucio estat dels casos d'us](
https://bytebucket.org/alexmancha/gps-up-26/raw/430459bfbe9123cc5f8b3be4f384202823838717/evolucio-casos-us-pla-fases.png)

L'evolució de l'estat de la resta de casos d'ús segueix un procés lògic i lineal, on els casos d'ús van evolucionant d'estat a mida que els diversos rols del projecte s'impliquen en cada fase. D'aquesta manera, per exemple, no és fins la fase de construcció que els desenvolupadors i els QA poden començar a donar per completat el desenvolupament dels casos d'ús.

La previsió, evidentment, és haver completat el desenvolupament dels diversos casos d'ús en la fase de transició. 

#### Objectius ####

![Objectius](https://bytebucket.org/alexmancha/gps-up-26/raw/088d682ae5e206fa176ab3c21d8accb771ab6b83/objectius.png)

#### Temps ####

![Temps](https://bytebucket.org/alexmancha/gps-up-26/raw/088d682ae5e206fa176ab3c21d8accb771ab6b83/temps.png)

## 4. RECURSOS ##

[1] https://www.glassdoor.com/Salaries/software-engineer-salary-SRCH_KO0,17.htm

[2] http://www.payscale.com/research/US/Job=Computer_Programmer/Salary

[3] https://www.tutorialspoint.com/estimation_techniques/estimation_techniques_use_case_points.htm
