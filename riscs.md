# SISTEMA UPC4ALL - RISCS #

## RISC 001. Resistència al canvi ##

### Descripció ###

> Els usuaris que han de fer servir el nou sistema poden mostrar-s'hi en contra per diversos motius, cosa que condicionaria l'èxit de la implamantació del projecte.

### Probabilitat ###

> Probable
 
### Impacte ###

> Molt gran, ja que un rebuig per part dels usuaris que han de fer servir el sistema implicaria un fracàs en la seva implantació. Difícilment una solució purament tècnica podria resoldre aquest risc.
 
### Indicadors ###

> Grau de satisfacció dels usuaris
 
### Estratègies de prevenció ###

> Explicació dels avantatges que ofereix el sistema sobre la situació actual, i formació específica del sistema per a usuaris clau.
 
### Plans de mitigació ###

> Buscar la manera per descobrir les causes del rebuig i intentar resoldre-les en futures entreges.

## RISC 002. Aparició de requisits inesperats ##

### Descripció ###

> Durant el desenvolupament del projecte poden aparèixer requisits funcionals o no funcionals que no estaven contemplats de bon principi.

### Probabilitat ###

> Molt probable
 
### Impacte ###

> Gran, sempre que la gestió del projecte no permeti modificacions en l'abast de la solució.
 
### Indicadors ###

> Desviacions en els terminis d'entrega si la velocitat de desenvolupament es manté.
 
### Estratègies de prevenció ###

> Difícilment es podran definir a priori els requisits que puguin anar apareixen a mesura que es desenvolupi el projecte, però sí que es pot fer un esforç per desgranar aquelles tasques que d'entrada semblin més complexes. Això permetrà reduir la incertesa en parts concretes del projecte.
 
### Plans de mitigació ###

> Validar cada cert temps els requisits inicials, per modificar-ne la planificació.


## RISC 003. Error de càlcul en el temps requerit ##

### Descripció ###

> La previsió de temps necessari per desenvolupar el projecte i el temps real necessari poden variar molt a llarg termini.

### Probabilitat ###

> Probable
 
### Impacte ###

> Gran, sempre que la temporalitat del projecte sigui un element clau pel seu èxit. En altres casos, l'impacte d'aquest risc pot ser menor.
 
### Indicadors ###

> Endarreriments en les fases dutes a terme del projecte.
 
### Estratègies de prevenció ###

> Preveure, per cada tasca, un període de temps superior a l'estimat.
 
### Plans de mitigació ###

> En cas de desviacions severes en la planificació, reduir l'abast del projecte per apropar-se a la data d'entrega definida incialment.

## RISC 004. Falta de suport per part dels actors implicats ##

### Descripció ###

> Els usuaris que han de fer servir el nou sistema poden mostrar-s'hi en contra per diversos motius, cosa que condicionaria l'èxit de la implamantació del projecte.

### Probabilitat ###

> Versemblant
 
### Impacte ###

> Gran, sobretot en funció de les necessitats de quin actor implicat no quedin reflectides en el projecte.
 
### Indicadors ###

> Grau de satisfacció dels actors implicats durant el desenvolupament del projecte, implicació dels mateixos a l'hora de definir-ne els requeriments.
 
### Estratègies de prevenció ###

> Identificar i tenir en compte les necessitats de tots els actors implicats des de l'inici del projecte, i específicament a la fase de recollida de requeriments.
 
### Plans de mitigació ###

> Mantenir contacte amb els actors implicats en el projecte.

## RISC 005. Dependència de tercers ##

### Descripció ###

> Algunes parts del projecte poden dependre de l'acció de terceres persones, organitzacions o entitats, que poden fer perillar la planificació temporal i de recursos del projecte.

### Probabilitat ###

> Versemblant
 
### Impacte ###

> Costa de resumir l'impacte en una etiqueta general, ja que algunes dependències poden resultar crítiques però altres es poden resoldre amb petites modificacions sobre la previsió del projecte.
 
### Indicadors ###

> Quantitat de tasques de l'equip de desenvolupament que queden aturades per dependències de tercers.
 
### Estratègies de prevenció ###

> Identificar les tasques on terceres persones, organitzacions o entitats tindran un paper imprescindible per a la seva resolució, i tenir-ho en compte a l'hora de planificar-les.
 
### Plans de mitigació ###

> En cas de detectar un problema de dependència externa, deixar la tasca aparcada fins que es pugui resoldre, i passar al següent punt que es pugui desenvolupar.